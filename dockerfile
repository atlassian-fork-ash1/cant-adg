FROM node:8-alpine

WORKDIR /opt/service

# Copy PJ, changes should invalidate entire image
COPY package.json yarn.lock /opt/service/

# Build backend
COPY src /opt/service/src

# Install dependencies
RUN yarn

EXPOSE 8880

ENTRYPOINT ["npm", "start", "--"]