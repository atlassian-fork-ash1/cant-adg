const getScores = (url) => {
  let xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
         console.log(xhttp.responseText);
         return xhttp.responseText;
      }
  };
  xhttp.open("GET", url, true);
  xhttp.send();
}

const postScores = (url, param) => {
  let xhttp = new XMLHttpRequest();
  xhttp.open("POST", url, true);
  //Send the proper header information along with the request
  xhttp.setRequestHeader('Content-type', 'application/json');

  xhttp.onreadystatechange = function() {//Call a function when the state changes.
      if(xhttp.readyState == 4 && xhttp.status == 200) {
          return xhttp.responseText;
      }
  }
  xhttp.send(param);
}
